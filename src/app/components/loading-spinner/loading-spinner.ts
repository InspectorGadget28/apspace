import { Component, Input } from '@angular/core';

@Component({
  selector: 'loading-spinner',
  templateUrl: 'loading-spinner.html',
  styleUrls: ['loading-spinner.scss'],
})
export class LoadingSpinnerComponent {
  @Input() message?: string;
  @Input() size?: string;
  @Input() textSize?: string;
  @Input() color ?= '#666666';
}
