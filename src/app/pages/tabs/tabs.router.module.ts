import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from '../../guards/auth.guard';
import { Role } from '../../interfaces';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        canActivate: [AuthGuard],
        // tslint:disable-next-line: no-bitwise
        data: { role: Role.Student | Role.Lecturer | Role.Admin },
        path: 'dashboard',
        loadChildren: () => import('../dashboard/dashboard.module').then( m => m.DashboardPageModule)
      },
      {
        canActivate: [AuthGuard],
        data: { role: Role.Student },
        path: 'student-timetable',
        loadChildren: () => import('../student-timetable/student-timetable.module').then(m => m.StudentTimetablePageModule)
      },
      {
        canActivate: [AuthGuard],
        data: { role: Role.Lecturer },
        path: 'lecturer-timetable',
        loadChildren: () => import('../lecturer-timetable/lecturer-timetable.module').then(m => m.LecturerTimetablePageModule)
      },
      {
        canActivate: [AuthGuard],
        data: { role: Role.Student },
        path: 'attendance',
        loadChildren: () => import('../attendance/attendance.module').then(m => m.AttendancePageModule)
      },
      {
        canActivate: [AuthGuard],
        // tslint:disable-next-line:no-bitwise
        data: { role: Role.Lecturer | Role.Admin },
        path: 'profile',
        loadChildren: () => import('../profile/profile.module').then(m => m.ProfilePageModule)
      },
      {
        canActivate: [AuthGuard],
        path: 'apcard',
        // tslint:disable-next-line:no-bitwise
        data: { role: Role.Student | Role.Lecturer | Role.Admin },
        loadChildren: () => import('../apcard/apcard.module').then(m => m.ApcardPageModule)
      },
      {
        canActivate: [AuthGuard],
        path: 'more',
        // tslint:disable-next-line:no-bitwise
        data: { role: Role.Student | Role.Lecturer | Role.Admin },
        loadChildren: () => import('../more/more.module').then(m => m.MorePageModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule { }
